﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.WebBrowser2 = New System.Windows.Forms.WebBrowser()
        Me.RTB1 = New System.Windows.Forms.RichTextBox()
        Me.dgv1 = New System.Windows.Forms.DataGridView()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.LBLvon = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.DTP_START = New System.Windows.Forms.DateTimePicker()
        Me.DTP_STOP = New System.Windows.Forms.DateTimePicker()
        Me.BtStart = New System.Windows.Forms.Button()
        Me.DTP_aktuell = New System.Windows.Forms.DateTimePicker()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.BtSaveXML = New System.Windows.Forms.Button()
        Me.BtSaveTXT = New System.Windows.Forms.Button()
        Me.SFD1 = New System.Windows.Forms.SaveFileDialog()
        CType(Me.dgv1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'WebBrowser2
        '
        Me.WebBrowser2.Location = New System.Drawing.Point(12, 12)
        Me.WebBrowser2.MinimumSize = New System.Drawing.Size(20, 20)
        Me.WebBrowser2.Name = "WebBrowser2"
        Me.WebBrowser2.ScriptErrorsSuppressed = True
        Me.WebBrowser2.Size = New System.Drawing.Size(615, 315)
        Me.WebBrowser2.TabIndex = 3
        '
        'RTB1
        '
        Me.RTB1.Location = New System.Drawing.Point(25, 334)
        Me.RTB1.Name = "RTB1"
        Me.RTB1.Size = New System.Drawing.Size(67, 336)
        Me.RTB1.TabIndex = 4
        Me.RTB1.Text = ""
        Me.RTB1.Visible = False
        '
        'dgv1
        '
        Me.dgv1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgv1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.dgv1.Location = New System.Drawing.Point(0, 333)
        Me.dgv1.Name = "dgv1"
        Me.dgv1.Size = New System.Drawing.Size(1584, 528)
        Me.dgv1.TabIndex = 5
        '
        'Timer1
        '
        Me.Timer1.Interval = 50
        '
        'LBLvon
        '
        Me.LBLvon.AutoSize = True
        Me.LBLvon.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LBLvon.Location = New System.Drawing.Point(652, 57)
        Me.LBLvon.Name = "LBLvon"
        Me.LBLvon.Size = New System.Drawing.Size(50, 25)
        Me.LBLvon.TabIndex = 10
        Me.LBLvon.Text = "von"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(652, 94)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(43, 25)
        Me.Label1.TabIndex = 12
        Me.Label1.Text = "bis"
        '
        'DTP_START
        '
        Me.DTP_START.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DTP_START.Location = New System.Drawing.Point(704, 52)
        Me.DTP_START.Name = "DTP_START"
        Me.DTP_START.Size = New System.Drawing.Size(411, 31)
        Me.DTP_START.TabIndex = 13
        '
        'DTP_STOP
        '
        Me.DTP_STOP.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DTP_STOP.Location = New System.Drawing.Point(704, 89)
        Me.DTP_STOP.Name = "DTP_STOP"
        Me.DTP_STOP.Size = New System.Drawing.Size(411, 31)
        Me.DTP_STOP.TabIndex = 14
        '
        'BtStart
        '
        Me.BtStart.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtStart.Location = New System.Drawing.Point(704, 126)
        Me.BtStart.Name = "BtStart"
        Me.BtStart.Size = New System.Drawing.Size(75, 34)
        Me.BtStart.TabIndex = 15
        Me.BtStart.Text = "Start"
        Me.BtStart.UseVisualStyleBackColor = True
        '
        'DTP_aktuell
        '
        Me.DTP_aktuell.Enabled = False
        Me.DTP_aktuell.Location = New System.Drawing.Point(633, 307)
        Me.DTP_aktuell.Name = "DTP_aktuell"
        Me.DTP_aktuell.Size = New System.Drawing.Size(200, 20)
        Me.DTP_aktuell.TabIndex = 16
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(633, 291)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(119, 13)
        Me.Label2.TabIndex = 17
        Me.Label2.Text = "Aktuelles Abfragedatum"
        '
        'BtSaveXML
        '
        Me.BtSaveXML.Location = New System.Drawing.Point(862, 304)
        Me.BtSaveXML.Name = "BtSaveXML"
        Me.BtSaveXML.Size = New System.Drawing.Size(117, 23)
        Me.BtSaveXML.TabIndex = 18
        Me.BtSaveXML.Text = "Save als .xml"
        Me.BtSaveXML.UseVisualStyleBackColor = True
        '
        'BtSaveTXT
        '
        Me.BtSaveTXT.Location = New System.Drawing.Point(998, 304)
        Me.BtSaveTXT.Name = "BtSaveTXT"
        Me.BtSaveTXT.Size = New System.Drawing.Size(117, 23)
        Me.BtSaveTXT.TabIndex = 19
        Me.BtSaveTXT.Text = "Save als .txt"
        Me.BtSaveTXT.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1584, 861)
        Me.Controls.Add(Me.BtSaveTXT)
        Me.Controls.Add(Me.BtSaveXML)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.DTP_aktuell)
        Me.Controls.Add(Me.BtStart)
        Me.Controls.Add(Me.DTP_STOP)
        Me.Controls.Add(Me.DTP_START)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.LBLvon)
        Me.Controls.Add(Me.dgv1)
        Me.Controls.Add(Me.RTB1)
        Me.Controls.Add(Me.WebBrowser2)
        Me.Name = "Form1"
        Me.Text = "Form1"
        CType(Me.dgv1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents WebBrowser2 As WebBrowser
    Friend WithEvents RTB1 As RichTextBox
    Friend WithEvents dgv1 As DataGridView
    Friend WithEvents Timer1 As Timer
    Friend WithEvents LBLvon As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents DTP_START As DateTimePicker
    Friend WithEvents DTP_STOP As DateTimePicker
    Friend WithEvents BtStart As Button
    Friend WithEvents DTP_aktuell As DateTimePicker
    Friend WithEvents Label2 As Label
    Friend WithEvents BtSaveXML As Button
    Friend WithEvents BtSaveTXT As Button
    Friend WithEvents SFD1 As SaveFileDialog
End Class

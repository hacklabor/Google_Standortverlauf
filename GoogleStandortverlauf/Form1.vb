﻿Imports System.Threading
Public Class Form1
    Dim web_geladen As String = ""
    Dim AbfrageTag, EndTag As Date
    Dim Zähler As Byte
    Dim DS_ALL As DataSet
    Dim tab_all As DataTable
    Dim cb_collums As New ComboBox
    Dim cb_doppelt As New ComboBox

    Private Sub BtStart_Click(sender As Object, e As EventArgs) Handles BtStart.Click
        DS_ALL = New DataSet
        tab_all = New DataTable
        cb_collums = New ComboBox
        cb_doppelt = New ComboBox
        DS_ALL.Tables.Add(tab_all)
        Dim col As New DataColumn
        col.ColumnName = "Start"
        col.DataType = System.Type.GetType("System.DateTime")
        tab_all.Columns.Add(col)
        col = New DataColumn
        col.ColumnName = "Ende"
        col.DataType = System.Type.GetType("System.DateTime")
        tab_all.Columns.Add(col)
        col = New DataColumn
        col.ColumnName = "DIFF"

        col.DataType = System.Type.GetType("System.TimeSpan")
        tab_all.Columns.Add(col)
        col = New DataColumn
        col.ColumnName = "12s"
        col.DataType = System.Type.GetType("System.String")
        tab_all.Columns.Add(col)
        cb_collums.Items.Add(col.ColumnName)
        col = New DataColumn
        col.ColumnName = "40s"
        col.DataType = System.Type.GetType("System.String")
        tab_all.Columns.Add(col)
        cb_collums.Items.Add(col.ColumnName)

        Me.BackColor = Color.Red
        AbfrageTag = DTP_START.Value
        EndTag = DTP_STOP.Value
        Timer1.Start()

    End Sub
    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        Dim Datum As String = AbfrageTag.ToString("yyyy-MM-dd")
        DTP_aktuell.Value = AbfrageTag
        WebBrowser2.Navigate("https://www.google.de/maps/timeline?pb=!1m2!1m1!1s" + Datum)

        Timer1.Stop()
    End Sub

    Private Sub WebBrowser2_DocumentCompleted(sender As Object, e As WebBrowserDocumentCompletedEventArgs) Handles WebBrowser2.DocumentCompleted

        daten_holen(AbfrageTag)

        If AbfrageTag.ToString("yyyy-MM-dd") <> EndTag.ToString("yyyy-MM-dd") Then
            AbfrageTag = AbfrageTag.AddDays(1)
            Timer1.Start()

        Else
            Me.BackColor = Color.Orange
        End If
    End Sub

    Private Sub daten_holen(ByRef datum As Date)
        RTB1.Text = WebBrowser2.DocumentText
        If RTB1.Text.IndexOf("var initialData=") > -1 Then
            Dim STR As String = RTB1.Lines(RTB1.Lines.Count - 1)
            Dim Data() As String = STR.Replace("10m2", "§").Split("§")
            RTB1.Text = ""
            Dim tmp As String = ""
            Dim leerzeile As Boolean = False
            Dim DS As Int16 = 0
            For i = 0 To Data.Length - 2
                If Data(i).IndexOf("!12s") > -1 And Data(i).IndexOf("!3s") > -1 And Data(i).IndexOf("!4s") > -1 Then
                    ds_all_füllen(Data(i))
                End If
            Next
        Else
            AbfrageTag = EndTag
        End If

    End Sub

    Private Sub ds_all_füllen(ByVal daten As String)
        Dim dat() As String = daten.Split("!")
        Dim Header As String = ""
        Dim nutzdaten As String = ""
        Dim row As DataRow = tab_all.NewRow
        For i = 0 To dat.Length - 1
            If dat(i).Length > 2 Then
                If IsNumeric(dat(i).Substring(0, 2)) = True Then
                    Header = dat(i).Substring(0, 3)
                Else
                    Header = dat(i).Substring(0, 2)
                End If
                nutzdaten = dat(i).Replace(Header, "")
                nutzdaten = nutzdaten.Replace("\x26", "&")
                nutzdaten = nutzdaten.Replace("\x27", "'")
                If cb_collums.Items.Contains(Header) = False Then
                    cb_collums.Items.Add(Header)
                    Dim col As New DataColumn
                    col.ColumnName = Header
                    col.DataType = System.Type.GetType("System.String")
                    tab_all.Columns.Add(col)
                End If
                row(Header) = nutzdaten

                If Header = "1j" Then
                    row(0) = UnixTimeStamp_to_Date(nutzdaten)
                    If cb_doppelt.Items.Contains(nutzdaten) = True Then
                        Return
                    Else
                        cb_doppelt.Items.Add(nutzdaten)
                    End If
                End If
            End If
            If Header = "2j" Then
                row(1) = UnixTimeStamp_to_Date(nutzdaten)
                row(2) = row(1) - row(0)
            End If

        Next
        tab_all.Rows.Add(row)
        dgv1.DataSource = DS_ALL
        dgv1.DataMember = tab_all.TableName
    End Sub



    Private Sub BtSaveXML_Click(sender As Object, e As EventArgs) Handles BtSaveXML.Click

        SFD1.Filter = "XML Datei|*.xml"
        If SFD1.ShowDialog = DialogResult.OK Then
            DS_ALL.WriteXml(SFD1.FileName)
        End If
    End Sub

    Private Sub BtSaveTXT_Click(sender As Object, e As EventArgs) Handles BtSaveTXT.Click
        Dim Trenner As String
        RTB1.Text = ""

        Trenner = InputBox("Trenner", "/t für TAB")
        If Trenner = "/t" Then Trenner = vbTab
        SFD1.Filter = "Text Datei|*.txt"
        If SFD1.ShowDialog = DialogResult.OK Then

            For i = 0 To tab_all.Columns.Count - 1
                RTB1.Text += tab_all.Columns(i).ColumnName + Trenner
            Next
            RTB1.Text += vbCrLf
            For R = 0 To tab_all.Rows.Count - 1
                Dim ro As DataRow = tab_all.Rows(R)
                For i = 0 To tab_all.Columns.Count - 1
                    RTB1.Text += ro(i) + Trenner
                Next
                RTB1.Text += vbCrLf
            Next
            RTB1.SaveFile(SFD1.FileName, RichTextBoxStreamType.PlainText)
            RTB1.SaveFile(SFD1.FileName, RichTextBoxStreamType.PlainText)
        End If
    End Sub

    Private Function UnixTimeStamp_to_Date(unixTime As String)

        Dim origin As Date = New DateTime(1970, 1, 1, 0, 0, 0, 0)
        Return origin.AddMilliseconds(unixTime).ToLocalTime

    End Function


End Class
